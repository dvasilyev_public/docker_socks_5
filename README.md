## Docker SOCKS5 Proxy


   ```bash
   git clone https://gitlab.com/dvasilyev_public/docker_socks_5.git
   cd docker_socks_5
   ```

   ```bash
   docker build -t socks5 .
   ```

   user:password in [Dockerfile](Dockerfile#L4) port 1080
   ```bash
   docker run -d -p 80:1080
   ```

   ```bash
   https://github.com/elejke/docker-socks5?tab=readme-ov-file

   git clone https://gitlab.com/dvasilyev_public/docker_socks_5.git
   cd docker_socks_5

   docker build -t socks5 .
   docker run -d -p 80:1080 socks5

   curl ipinfo.io
   curl --proxy socks5://193.108.114.55:80 ipinfo.io
   curl https://lkk.mosobleirc.ru

   ping "api.huobi.pro"
   ping "openapi-v2.kucoin.com"
   ping "api.binance.com"
   ```
